from django.db.models import Q
from rest_framework import filters


class IsOwnerFilterBackend(filters.BaseFilterBackend):
    """
    Filter for getting only owned objects
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(created_by=request.user)