from rest_framework import serializers
from .models import Task, Tags, TaskInvite
from user.serializer import UserSerializer
from user.models import User


class TagsSerializer(serializers.Serializer):
    class Meta:
        model = Tags
        fields = ('__all__')


class TaskSerializer(serializers.ModelSerializer):
    created_by = UserSerializer()

    # tags = TagsSerializer(many=True, read_only=True)

    class Meta:
        model = Task
        fields = ('__all__')


class TaskSerializerPost(serializers.ModelSerializer):
    # created_by = UserSerializer()
    class Meta:
        model = Task
        # fields = ('__all__')
        exclude = ('created_by',)


from django.contrib.auth.password_validation import validate_password


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    # def validate_new_password(self, value):
    #     validate_password(value)
    #     return value


class TaskInviteeSerializer(serializers.Serializer):
    class Meta:
        model = TaskInvite
        fields = ('__all__')
