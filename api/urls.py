from django.urls import path
from . import views
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('task-list', views.taskListView)
router.register('invite-user',views.InviteView)

urlpatterns = [
    path('', include(router.urls)),
    path('update-password/', views.UpdatePassword.as_view()),
    # path('invite-user/', views.InviteView.as_view()),
    # path('user/login/', SignInView.as_view(), name='SignIn'),

    # path('task-tag-list/', views.TaskTagsList, name="task-tags-list"),
    # path('task-detail/<str:pk>/', views.taskDetail, name="task-detail"),
    # path('task-create/', views.taskCreate, name="task-create"),
    #
    # path('task-update/<str:pk>/', views.taskUpdate, name="task-update"),
    # path('task-delete/<str:pk>/', views.taskDelete, name="task-delete"),
]
