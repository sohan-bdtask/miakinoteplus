from django.contrib import admin

# Register your models here.

from .models import Task, Tags, TaskInvite

admin.site.register(Task)
admin.site.register(Tags)
admin.site.register(TaskInvite)
