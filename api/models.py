from django.db import models
from user.models import User


# Create your models here.


class Tags(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Task(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=1000, blank=True)
    date_created = models.DateField(auto_now_add=True)
    completed = models.BooleanField(default=False, blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tags, related_name='tags_all', blank=True)

    def __str__(self):
        return self.title


class TaskInvite(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_viewed = models.BooleanField(default=False)
    invited_at = models.DateField(auto_now_add=True)

# class TaskTags(models.Model):
#     tags = models.ForeignKey(Tags, on_delete=models.CASCADE)
#     task = models.ForeignKey(Task, on_delete=models.CASCADE)
#
#     def __str__(self):
#         return self.task.title
