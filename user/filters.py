from django.db.models import Q
from rest_framework import filters


class IsOwnerFilterBackendProfile(filters.BaseFilterBackend):
    """
    Filter for getting only owned objects
    """

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(user_id=request.user)