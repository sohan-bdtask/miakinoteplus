from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import UserListView, ProfileView, registration_view, UserView

router = DefaultRouter()
router.register('userlist', UserListView)
router.register('profile', ProfileView)
router.register('user', UserView)
# router.register('register', registration_view, base_name='register')


urlpatterns = [
    path('', include(router.urls)),
]
