# Create your views here.
from django.shortcuts import render
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from .permission import IsOwnerOnly, IsOwnerOnlyProfile
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from . import models
from .serializer import RegistrationSerializer, UserSerializer, ProfileSerializer
from .filters import IsOwnerFilterBackendProfile
from .models import User


class OwnerMixin(object):
    permission_classes = (IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackendProfile,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserAdminViewSet(viewsets.ModelViewSet):
    """
    Convenience API view for viewing all data for the admin
    and all operations
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class ProfileAdminViewSet(viewsets.ModelViewSet):
    """
    Convenience API view for viewing all data for the admin
    and all operations
    """
    queryset = models.Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsAdminUser,)


@api_view(['POST', ])
def registration_view(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['response'] = "succesfully registered a new user"
            data['id'] = account.id
            data['email'] = account.email
            data['status'] = status.HTTP_201_CREATED
        else:
            data['status'] = status.HTTP_208_ALREADY_REPORTED
        return Response(data, status=data['status'])


class UserListView(viewsets.ModelViewSet):
    """
    List of user,details of user
`   """
    queryset = models.User.objects.all()
    serializer_class = UserSerializer

    # authentication_classes = [SessionAuthentication, JWTAuthentication]
    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(self.object_list, many=True)
        return Response({'userList': serializer.data})


class UserView(viewsets.ModelViewSet):
    """
    Current user
    """
    authentication_classes = [JWTAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnly]

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        print(self.object_list[0])
        serializer = self.get_serializer(self.object_list[0])
        return Response({'currentUser': serializer.data})


class ProfileView(OwnerMixin, ProfileAdminViewSet):
    """
    Current user-profile
    """
    authentication_classes = [JWTAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnlyProfile]

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(self.object_list[0])
        return Response({'currentUserProfile': serializer.data})

